package com.redditclient;

import android.content.Context;

import com.redditclient.view.LoginActivity;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * Created by deshraj.sharma on 8/28/2018.
 */

/**
 * This is the Mockito class for JUnit Testing
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class LoginUnitTest {

    /**
     * Variables
     */
    private String FAKE_STRING = "Login was successful";
    LoginActivity myObjectUnderTest = null;
    @Mock
    Context mMockContext;

    @Test
    public void callLoginMethod() {
        getInstrumentation().runOnMainSync(new Runnable() {
            @Override
            public void run() {
                myObjectUnderTest = new LoginActivity(mMockContext);
//                activity = startActivity(new Intent(Intent.ACTION_MAIN), null, null);
            }
        });


        // ...when the string is returned from the object under test...
        String result = myObjectUnderTest.validate("user");

        // ...then the result should be the expected one.

        assertThat(result, is(FAKE_STRING));

    }

}