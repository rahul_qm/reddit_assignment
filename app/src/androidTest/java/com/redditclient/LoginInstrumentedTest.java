package com.redditclient;

import android.test.ActivityInstrumentationTestCase2;
import android.widget.EditText;

import com.redditclient.view.LoginActivity;

/**
 * Created by deshraj.sharma on 8/28/2018.
 *
 * This class holds the test case for the Login Activity
 */


public class LoginInstrumentedTest extends
        ActivityInstrumentationTestCase2<LoginActivity> {

    /**
     * Initiate the Variables
     */
    private LoginActivity mActivity;
    private EditText mView;
    private String resourceString;

    public LoginInstrumentedTest() {

        super(LoginActivity.class);
    }

    /**
     * Method to Initiate the view
     * @throws Exception
     */
    @Override
    protected void setUp() throws Exception {
        super.setUp();
        mActivity = this.getActivity();
        mView =  mActivity.findViewById
                (R.id.edtUsername);
        resourceString = mActivity.getString
                (R.string.enter_username);
    }

    public void testPreconditions() {
        assertNotNull(mView);
    }

    /**
     * Method the test the validation
     */
    public void testText() {
        assertEquals(resourceString, mView.getText().toString());
    }
}