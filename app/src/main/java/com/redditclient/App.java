package com.redditclient;

import android.app.Application;
import android.databinding.DataBindingUtil;

import com.redditclient.databinding.AppDataBindingComponent;

public class App extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        DataBindingUtil.setDefaultComponent(new AppDataBindingComponent());
    }
}
