package com.redditclient.interfaces;

/**
 * Created by deshraj.sharma on 8/28/2018.
 */

public interface LoginCallbackListener {

    void onLoginSuccess(String message);

    void onLoginError(String message);
}
