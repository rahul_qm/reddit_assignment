package com.redditclient.interfaces;

import com.redditclient.model.RedditPostListModel;

/**
 * Created by deshraj.sharma on 8/28/2018.
 */

public interface GetPostListCallbacks {

    void onPostFetched(RedditPostListModel redditPostListModel);

    void onFailure(String message);
}
