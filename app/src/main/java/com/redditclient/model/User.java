package com.redditclient.model;

import android.text.TextUtils;

/**
 * Created by deshraj.sharma on 8/28/2018.
 */

public class User {

    String userName = "";

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public boolean isUserNameValid() {
        boolean isUserNameValid = false;

        if (!TextUtils.isEmpty(userName) && userName.length() <= 20) {
            isUserNameValid = true;
        }
        return isUserNameValid;
    }
}
