package com.redditclient.model.postdetails;



import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Data_  {


    @SerializedName("title")
    @Expose
    private String title;


    @SerializedName("name")
    @Expose
    private String name;


    @SerializedName("thumbnail")
    @Expose
    private String thumbnail;


    @SerializedName("preview")
    @Expose
    private Preview preview;

    @SerializedName("body")
    @Expose
    private String body;

    @SerializedName("author")
    @Expose
    private String author;


    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;

    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public Preview getPreview() {
        return preview;
    }

    public void setPreview(Preview preview) {
        this.preview = preview;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
}
