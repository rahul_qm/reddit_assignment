package com.redditclient.network;

import com.redditclient.model.RedditPostListModel;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Url;

/**
 * Created by deshraj.sharma on 4/18/2018.
 */

public interface PostListService {
    @GET
    Observable<RedditPostListModel> getPostList(@Url String url);
}
