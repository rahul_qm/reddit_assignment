package com.redditclient.network;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by deshraj.sharma on 4/2/2018.
 */

/**
 * This is our util class to handle the HTTP requests
 */
public class RetroFactoryUtil {

    public static final String BASE_URL = "https://www.reddit.com/";

    public static final String POSTLIST_URL = BASE_URL + "r/all.json";
    private static RetroFactoryUtil mRetrofFactoryUtil;
    private static Retrofit mRetrofitInstance;

    private static Retrofit getRetrofitInstance() {
        if (mRetrofitInstance == null) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();

            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

            mRetrofitInstance = new Retrofit.Builder().baseUrl(BASE_URL).client(client)
                    .addConverterFactory(
                            GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build();
        }
        return mRetrofitInstance;
    }


    public static PostListService createRedditPostListService() {
        return getRetrofitInstance().create(PostListService.class);
    }

    public static PostDetailsService createRedditPostDetailsService() {
        return getRetrofitInstance().create(PostDetailsService.class);
    }

    public static RetroFactoryUtil getInstance() {
        {
            if (mRetrofFactoryUtil == null) {

                mRetrofFactoryUtil = new RetroFactoryUtil();
            }
            return mRetrofFactoryUtil;
        }
    }


}
