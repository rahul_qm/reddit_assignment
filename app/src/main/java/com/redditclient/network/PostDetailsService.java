package com.redditclient.network;

import com.redditclient.model.postdetails.PostDetialsModel;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Url;

/**
 * Created by deshraj.sharma on 4/18/2018.
 */

public interface PostDetailsService {
    @GET
    Observable<List<PostDetialsModel>> getPostDetails(@Url String url);
}
