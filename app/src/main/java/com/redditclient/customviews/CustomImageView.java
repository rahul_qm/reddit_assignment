package com.redditclient.customviews;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.ProgressBar;

import com.redditclient.R;
import com.squareup.picasso.Picasso;


/**
 * This class is used as widget instead to using FrameLayout with ProgressBar
 * and ImageView. This is used like a ImageView. This is used when images are
 * fetched from server and show on ImageView. Till the images are loader from
 * server progress bar is visible on image view. After that Images are loaded
 * and set on imageView. This is also done with the help of Universal Image
 * Loader.
 */
public class CustomImageView extends FrameLayout {

    private ImageView mAsyncImageView;
    private ProgressBar mProgressBar;

    private int minHeight, minWidth;
//    private byte[] mLoadedBitmapBytArray = null;

    public CustomImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        // TODO Auto-generated constructor stub
        initView();
        if (attrs != null) {
            TypedArray array = context.obtainStyledAttributes(attrs,
                    R.styleable.CustomImageView, 0, 0);
            String url = array.getString(R.styleable.CustomImageView_url);
            int defaultImage = array.getResourceId(
                    R.styleable.CustomImageView_defaultImage, 0);
            minHeight = array.getInt(R.styleable.CustomImageView_minHeight, 0);
            minWidth = array.getInt(R.styleable.CustomImageView_minWidth, 0);

            if (!isEmptyOrNull(url)) {
                setImageUrl(url, defaultImage);
            }

            if (minWidth > 0 && minHeight > 0) {
                LayoutParams param = new LayoutParams(minWidth, minHeight);
                mAsyncImageView.setLayoutParams(param);
            }
            array.recycle();
        }
    }

    public CustomImageView(Context context) {
        super(context);
        // TODO Auto-generated constructor stub
        initView();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        mAsyncImageView.setAdjustViewBounds(true);
        if (minHeight != 0)
            mAsyncImageView.getLayoutParams().height = (int) minHeight;
        if (minWidth != 0)
            mAsyncImageView.getLayoutParams().width = (int) minWidth;
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    private void initView() {
        LayoutInflater inflator = LayoutInflater.from(getContext());
        View v = inflator.inflate(R.layout.widget_custom_image_view, this);
        mProgressBar = (ProgressBar) v
                .findViewById(R.id.widget_custom_image_view_progress_bar);
        mAsyncImageView = (ImageView) v.findViewById(R.id.widget_custom_image_view_image);
        mAsyncImageView.setScaleType(ScaleType.FIT_XY);

    }

    public Bitmap getBitmap() {
        return ((BitmapDrawable) mAsyncImageView.getDrawable()).getBitmap();
    }


    /**
     * This method is used to setImage on ImageView.
     *
     * @param url set url of image
     */

    public final void setImageUrl(String url, int defaultImage) {
        Picasso.with(getContext())
                .load(url)
                .placeholder(defaultImage)
                .into(mAsyncImageView);
    }


    private boolean isEmptyOrNull(String str) {
        return !(!TextUtils.isEmpty(str) && !str.equals("null"));
    }

}
