package com.redditclient.view;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.redditclient.R;
import com.redditclient.databinding.ActivityPostDetailsBinding;
import com.redditclient.viewmodel.PostDetailViewModel;

/**
 * Created by deshraj.sharma on 8/28/2018.
 */

public class PostDetailsActivity extends AppCompatActivity {
    private String postLink;
    private PostDetailViewModel postDetailViewModel;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityPostDetailsBinding activityPostDetailsBinding = DataBindingUtil.setContentView(this, R.layout.activity_post_details);
        PostDetailViewModel postDetailViewModel = new PostDetailViewModel(this);
        activityPostDetailsBinding.setPostdetailviewmodel(postDetailViewModel);
        if (getIntent() != null) {
            if (getIntent().hasExtra("PostLink")) {
                postLink = getIntent().getStringExtra("PostLink");
                postDetailViewModel.getPostDetails(postLink);
            }
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(postDetailViewModel != null)
        {
            postDetailViewModel.reset();
        }
    }
}
