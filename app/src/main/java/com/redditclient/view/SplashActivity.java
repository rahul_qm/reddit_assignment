package com.redditclient.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.redditclient.R;
import com.redditclient.utils.SharedPreferencesUtils;

/**
 * Created by deshraj.sharma on 8/28/2018.
 */

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        if (SharedPreferencesUtils.getInstance(this).getBoolean("IsLoginSuccess", false)) {
            startActivity(new Intent(this, PostListActivity.class));
        } else {
            startActivity(new Intent(this, LoginActivity.class));
        }
        finish();
    }
}
