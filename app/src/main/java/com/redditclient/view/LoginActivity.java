package com.redditclient.view;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.redditclient.R;
import com.redditclient.databinding.ActivityLoginBinding;
import com.redditclient.interfaces.LoginCallbackListener;
import com.redditclient.utils.SharedPreferencesUtils;
import com.redditclient.viewmodel.LoginViewModel;

/**
 * Created by deshraj.sharma on 8/28/2018.
 */

public class LoginActivity extends AppCompatActivity implements LoginCallbackListener {

    private LoginViewModel loginViewModel;

    /**
     * Parameterized construct created to test Mockito
     * @param context
     */
    public LoginActivity(Context context) {

    }

    /**
     * Non Parameterized construct created to test Instrumentation
     *
     */
    public LoginActivity() {

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityLoginBinding activityLoginBinding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        activityLoginBinding.setLoginViewModel(new LoginViewModel(this));

    }

    public LoginViewModel getLoginViewModel() {
        return loginViewModel;
    }

    /**
     * To test with Mockito
     *
     * @param userName
     * @return
     */
    public String validate(String userName) {
        if (userName.equals("user"))
            return "Login was successful";
        else
            return "Invalid login!";
    }

    /**
     * Callback method for Login Success
     * @param message
     */
    @Override
    public void onLoginSuccess(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        SharedPreferencesUtils.getInstance(this).putBoolean("IsLoginSuccess", true);
        startActivity(new Intent(this, PostListActivity.class));
        finish();
    }

    /**
     * Callback method for Login Failure
     * @param message
     */

    @Override
    public void onLoginError(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
