package com.redditclient.view;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import com.redditclient.R;
import com.redditclient.adapter.DataAdapter;
import com.redditclient.databinding.ActivityPostListBinding;
import com.redditclient.interfaces.GetPostListCallbacks;
import com.redditclient.model.Child;
import com.redditclient.model.RedditPostListModel;
import com.redditclient.utils.RecyclerTouchListener;
import com.redditclient.viewmodel.DataViewModel;

import static android.support.v7.widget.LinearLayoutManager.VERTICAL;

/**
 * Created by deshraj.sharma on 8/28/2018.
 */

public class PostListActivity extends AppCompatActivity implements GetPostListCallbacks {
    private DataViewModel dataViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = bind();
        initRecyclerView(view);
    }

    @Override
    protected void onResume() {
        super.onResume();
        dataViewModel.getPostList();
    }

    @Override
    protected void onPause() {
        super.onPause();
        dataViewModel.tearDown();
    }

    private View bind() {

        ActivityPostListBinding activityPostListBinding = DataBindingUtil.setContentView(this, R.layout.activity_post_list);
        dataViewModel = new DataViewModel(this);
        activityPostListBinding.setViewModel(dataViewModel);

        return activityPostListBinding.getRoot();
    }

    private void initRecyclerView(View view) {
        RecyclerView recyclerView = view.findViewById(R.id.data_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        recyclerView.addItemDecoration(new DividerItemDecoration(recyclerView.getContext(), VERTICAL));

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view,Child child, int position) {
                if (child != null) {
                    Intent intent = new Intent(PostListActivity.this, PostDetailsActivity.class);
                    intent.putExtra("PostLink", child.getData().getPermalink());
                    startActivity(intent);
                }

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));


    }

    @Override
    public void onPostFetched(RedditPostListModel redditPostListModel) {
        Toast.makeText(this, redditPostListModel.getData().getChildren().get(0).getData().getAuthor(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(dataViewModel != null)
        {
            dataViewModel.reset();
        }
    }
}
