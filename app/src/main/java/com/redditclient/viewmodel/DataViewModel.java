/*
 * Copyright (c) 2018 Phunware Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.redditclient.viewmodel;

import android.app.ProgressDialog;
import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.redditclient.BR;
import com.redditclient.adapter.DataAdapter;
import com.redditclient.model.Child;
import com.redditclient.model.RedditPostListModel;
import com.redditclient.network.PostListService;
import com.redditclient.network.RetroFactoryUtil;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/**
 *
 */
public class DataViewModel extends BaseObservable {
    private DataAdapter adapter = new DataAdapter();
    private List<Child> data = new ArrayList<>();
    private CompositeDisposable mCompositeDisposable = new CompositeDisposable();
    private Scheduler scheduler;
    private ProgressDialog mProgressDialog;
    private Context mContext;

    public DataViewModel(Context context) {
        mContext = context;
        data = new ArrayList<>();
        adapter = new DataAdapter();
        mProgressDialog = new ProgressDialog(mContext);
    }

    public void getPostList() {
        setProgressDialogMessage("Please wait...");
        showProgressDialog();
        PostListService postListService = RetroFactoryUtil.getInstance().createRedditPostListService();
        Disposable disposable = postListService.getPostList(RetroFactoryUtil.POSTLIST_URL)
                .subscribeOn(subscribeScheduler()).
                        observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<RedditPostListModel>() {
                    @Override
                    public void accept(RedditPostListModel redditPostListModel) throws Exception {

                        Log.e("Post fetched", redditPostListModel.getData().getChildren().get(0).getData().getName());
                        populateData(redditPostListModel);
                        dismissProgressDialog();
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Log.e("Post failed", throwable.getLocalizedMessage());

                        dismissProgressDialog();
                    }
                });
        getmCompositeDisposable().add(disposable);
    }


    public void tearDown() {
        // perform tear down tasks, such as removing listeners
    }

    @Bindable
    public List<Child> getData() {
        return this.data;
    }

    @Bindable
    public DataAdapter getAdapter() {
        return this.adapter;
    }

    private void populateData(RedditPostListModel redditPostListModel) {
        // populate the data from the source.
        data.clear();
        data.addAll(redditPostListModel.getData().getChildren());
        notifyPropertyChanged(BR.data);
    }


    public void setProgressDialogMessage(String message) {
        mProgressDialog.setMessage(message);
    }

    public void showProgressDialog() {
        mProgressDialog.show();
    }

    public void dismissProgressDialog() {
        if (mProgressDialog != null) {
            if (mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
            }

        }
    }

    public Scheduler subscribeScheduler() {
        if (scheduler == null) {
            scheduler = Schedulers.io();
        }

        return scheduler;
    }

    public void unSubscribeFromObservable() {
        if (mCompositeDisposable != null && !mCompositeDisposable.isDisposed()) {
            mCompositeDisposable.dispose();
        }
    }

    public void reset() {
        unSubscribeFromObservable();
        resetCompositeDrawable();
        ((AppCompatActivity) mContext).finish();
        mContext = null;
    }

    public CompositeDisposable getmCompositeDisposable() {
        return mCompositeDisposable;
    }

    public void resetCompositeDrawable() {
        mCompositeDisposable = null;
    }
}
