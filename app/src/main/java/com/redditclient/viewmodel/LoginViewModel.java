package com.redditclient.viewmodel;

import android.widget.EditText;
import com.redditclient.interfaces.LoginCallbackListener;
import com.redditclient.model.User;

/**
 * Created by deshraj.sharma on 8/28/2018.
 */

public class LoginViewModel {

    private User user;
    private LoginCallbackListener loginCallbackListener;

    public LoginViewModel(LoginCallbackListener loginCallbackListener) {

        this.loginCallbackListener = loginCallbackListener;
        user = new User();
    }

    public void callLoginMethod(EditText editUser) {
        String userName = editUser.getText().toString().trim();
        user.setUserName(userName);
        if (user.isUserNameValid()) {
            loginCallbackListener.onLoginSuccess("Login Success");
        } else {
            loginCallbackListener.onLoginSuccess("Login Failure");
        }
    }

}
