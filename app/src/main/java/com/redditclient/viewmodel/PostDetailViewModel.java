package com.redditclient.viewmodel;

import android.app.ProgressDialog;
import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.BindingAdapter;
import android.databinding.ObservableField;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.redditclient.R;
import com.redditclient.customviews.CustomImageView;
import com.redditclient.model.postdetails.PostDetialsModel;
import com.redditclient.network.PostDetailsService;
import com.redditclient.network.RetroFactoryUtil;

import java.util.List;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by deshraj.sharma on 8/28/2018.
 */

public class PostDetailViewModel extends BaseObservable {


    final ObservableField<String> url =
            new ObservableField<>();
    final ObservableField<String> author =
            new ObservableField<>();
    final ObservableField<String> title =
            new ObservableField<>();
    final ObservableField<String> body =
            new ObservableField<>();
    private CompositeDisposable mCompositeDisposable = new CompositeDisposable();
    private Scheduler scheduler;
    private ProgressDialog mProgressDialog;
    private Context mContext;

    public PostDetailViewModel(Context context) {
        mContext = context;
        mProgressDialog = new ProgressDialog(mContext);
    }

    @BindingAdapter({"bind:image"})
    public static void loadImage(CustomImageView view, String imageUrl) {
        view.setImageUrl(imageUrl, R.drawable.ic_register_image_default, false);
    }

    public ObservableField<String> getAuthor() {
        return author;
    }

    public ObservableField<String> getBody() {
        return body;
    }

    public ObservableField<String> getTitle() {
        return title;
    }

    public ObservableField<String> getUrl() {
        return url;
    }

    public void getPostDetails(String permalink) {
        setProgressDialogMessage("Please wait...");
        showProgressDialog();
        PostDetailsService postDetailsService = RetroFactoryUtil.getInstance().createRedditPostDetailsService();
        Disposable disposable = postDetailsService.getPostDetails(RetroFactoryUtil.BASE_URL + permalink + ".json")
                .subscribeOn(subscribeScheduler()).
                        observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<List<PostDetialsModel>>() {
                    @Override
                    public void accept(List<PostDetialsModel> postDetialsModel) throws Exception {
                        url.set(postDetialsModel.get(0).getData().getChildren().get(0).getData().getPreview().getImages().get(0).getSource().getUrl());
                        title.set(postDetialsModel.get(0).getData().getChildren().get(0).getData().getTitle());
                        author.set(postDetialsModel.get(0).getData().getChildren().get(0).getData().getAuthor());
                        try {
                            body.set(postDetialsModel.get(0).getData().getChildren().get(0).getData().getBody());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        dismissProgressDialog();
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Toast.makeText(mContext, throwable.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                        Log.e("Post failed", throwable.getLocalizedMessage());
                        dismissProgressDialog();
                    }
                });
        getmCompositeDisposable().add(disposable);
    }


    public void setProgressDialogMessage(String message) {
        mProgressDialog.setMessage(message);
    }

    public void showProgressDialog() {
        mProgressDialog.show();
    }

    public void dismissProgressDialog() {
        if (mProgressDialog != null) {
            if (mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
            }

        }
    }

    public Scheduler subscribeScheduler() {
        if (scheduler == null) {
            scheduler = Schedulers.io();
        }

        return scheduler;
    }

    public void unSubscribeFromObservable() {
        if (mCompositeDisposable != null && !mCompositeDisposable.isDisposed()) {
            mCompositeDisposable.dispose();
        }
    }

    public void reset() {
        unSubscribeFromObservable();
        resetCompositeDrawable();
        ((AppCompatActivity) mContext).finish();
        mContext = null;
    }

    public CompositeDisposable getmCompositeDisposable() {
        return mCompositeDisposable;
    }

    public void resetCompositeDrawable() {
        mCompositeDisposable = null;
    }
}
